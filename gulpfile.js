var gulp = require("gulp");
var ts = require("gulp-typescript");
var tsProject = ts.createProject("tsconfig.json");
var watch = require('gulp-watch');
const { spawn } = require('child_process');

gulp.task("default", function () {
    return tsProject.src()
        .pipe(tsProject())
        .js.pipe(gulp.dest("dist"));
});
gulp.task("scripts", function () {
    return tsProject.src()
        .pipe(tsProject())
        .js.pipe(gulp.dest("dist"));
});
gulp.task('run_server', ()=> {
    const run = spawn('node', [' dist\\server.js']);

    run.stdout.on('data', (data) => {
    console.log(`stdout: ${data}`);
    });

    run.stderr.on('data', (data) => {
    console.log(`stderr: ${data}`);
    });

    run.on('close', (code) => {
    console.log(`child process exited with code ${code}`);
    });
})

gulp.task('watch', ['default',], function() {
    gulp.watch( "src/**/*.ts", ['default',]);
});
