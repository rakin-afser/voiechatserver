import { Schema, model, Document } from "mongoose";
import { BaseTime, preSaveAddBaseTime } from "./base";
import * as bcrypt from "bcrypt";

const BCRYPT_SALT_WORK_FACTOR = 10;
export enum SEX {
  MALE = "MALE",
  FEMALE = "FEMALE"
}

export interface User {
  name: string;
  sex: SEX;
  avator: string;
  phoneNumber: string;
  accountID: string;
  appId: string;
  fbToken: string;
  countryCode: string;
}

export interface UserModel extends User, BaseTime, Document {
  comparePassword(password: string): boolean;
}

const modelSchema = new Schema({
  phoneNumber: { type: String, required: true, index: { unique: true } },
  countryCode: { type: String, required: true },
  fbToken: { type: String, required: true },
  appId: { type: String, required: true },
  accountID: { type: Date, required: true },
  name: { type: String, default: false },
  sex: { type: String },
  avator: { type: String }
});

modelSchema.pre("save", preSaveAddBaseTime);

////// Create Model /////

export const UserModel = model<UserModel>("User", modelSchema);

////// Functions ////////

export function getUsers(limit = 100) {
  return UserModel.find().limit(limit);
}

export function getUserById(id: string) {
  return UserModel.findOne({ _id: id });
}

export function getUserByPhone(phoneNumber) {
  return UserModel.findOne({ phoneNumber });
}

export async function addUser(input: User) {
  const rec = await UserModel.create(input);
  return rec;
}
export function removeUser(id) {
  console.log(id);

  return UserModel.findByIdAndRemove(id.id);
}
