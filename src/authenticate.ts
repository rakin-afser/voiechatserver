// @ts-ignore
import * as passport from "passport";
import * as jwt from "jsonwebtoken";
import { Application, Request, Response, RequestHandler } from "express";
import {
  Strategy,
  StrategyOptions,
  ExtractJwt,
  VerifiedCallback
} from "passport-jwt";
import { User, getUserById, getUserByPhone, addUser, SEX } from "./db/users";
import { PRIVATE_KEY, LOCAL_STORAGE } from "./constants/index";
import * as cors from "cors";
import { decrypt } from "./helper/encryptDecrypt";
interface JwtPayload {
  userId: string;
}

/**
 * Authorized User Store
 */

let authorizedUser = [];
let localstorage;
if (typeof localstorage === "undefined" || localstorage === null) {
  var LocalStorage = require("node-localstorage").LocalStorage;
  localstorage = new LocalStorage("./voicechat");
  // localstorage.removeItem(LOCAL_STORAGE);
  if (JSON.parse(localstorage.getItem(LOCAL_STORAGE)) != null) {
    console.log("----- ", JSON.parse(localstorage.getItem(LOCAL_STORAGE)));

    authorizedUser = JSON.parse(localstorage.getItem(LOCAL_STORAGE));
  }
}

const JWT_SECRET_KEY = PRIVATE_KEY;

/** Options for JWT
 * in this exemple we use fromAuthHeader, so the client need to
 * provide an "Authorization" request header token
 */
const jwtOptions: StrategyOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeader(),
  secretOrKey: JWT_SECRET_KEY,
  passReqToCallback: true
};

const jwtStategy = new Strategy(
  jwtOptions,
  (req: Request, jwtPayload: JwtPayload, done: VerifiedCallback) => {
    // In the login we encrypt the payload

    if (!jwtPayload.userId) {
      throw new Error("No userId in the JWT session token");
    }
    // const user = await getUser(username);
    if (authorizedUser.indexOf(jwtPayload.userId.toString()) > -1) {
      return done(null, jwtPayload.userId.toString());
    } else {
      return done(null, false);
    }
  }
);

/**
 * If added to a express route, it will make the route require the auth token
 */
export function onlyAuthorized() {
  return passport.authenticate("jwt", { session: false });
}

/**
 * Setup the Passport JWT for the given express App.
 * It will add the auth routes (auth, login, logout) to handle the token authorization
 * It will use the mongoDB UserModel to check for user and password
 * Set addDebugRoutes to true for adding a Auth Form for testing purpose
 */
export function setupPassportAuth(app: Application, addDebugRoutes = false) {
  app.use(cors({ credentials: true, origin: true }));

  passport.use(jwtStategy);
  app.use(passport.initialize());

  app.post("/login", async (req, res, next) => {
    try {
      const {
        name,
        sex,
        avator,
        phoneNumber,
        accountID,
        appId,
        countryCode,
        fbToken
      } = req.body;
      if (!phoneNumber || !accountID) {
        return res.json({
          error: "Phone Number & FB-Account ID not set in the request"
        });
      }
      let jwtPayload: JwtPayload;
      let token;
      // Get user by Phone Number
      const existingUser = await getUserByPhone(phoneNumber);
      if (!existingUser) {
        // Create new User
        const newUser: User = {
          phoneNumber: phoneNumber,
          sex: sex,
          avator: avator,
          name: name,
          accountID: accountID,
          appId: appId,
          countryCode: countryCode,
          fbToken: fbToken
        };
        addUser(newUser).then(response => {
          authorizedUser.push(response._id.toString());
          localstorage.setItem(LOCAL_STORAGE, JSON.stringify(authorizedUser));
          jwtPayload = {
            userId: response._id.toString()
          };
          token = jwt.sign(jwtPayload, jwtOptions.secretOrKey);
          return res.json({
            token,
            user: response
          });
        });
      } else {
        console.log(authorizedUser, "=== ", existingUser);

        authorizedUser.push(existingUser._id.toString());
        localstorage.setItem(LOCAL_STORAGE, JSON.stringify(authorizedUser));
        jwtPayload = {
          userId: existingUser._id.toString()
        };
        token = jwt.sign(jwtPayload, jwtOptions.secretOrKey);
        return res.json({
          token,
          user: existingUser
        });
      }
    } catch (error) {
      res.json({
        error: error.message
      });
    }
  });

  app.get("/logout", (req, res) => {
    req.logout();
    res.redirect("/");
  });
}
