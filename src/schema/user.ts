import {
  GraphQLEnumType,
  GraphQLInterfaceType,
  GraphQLObjectType,
  GraphQLList,
  GraphQLNonNull,
  GraphQLSchema,
  GraphQLString,
  GraphQLInt,
  GraphQLID,
  GraphQLBoolean
} from "graphql";
const SEXtype = new GraphQLEnumType({
  name: "SEX",
  values: {
    MALE: { value: "MALE" },
    FEMALE: { value: "FEMALE" }
  }
});
import {
  getUsers,
  getUserById,
  getUserByPhone,
  addUser,
  removeUser
} from "../db/users";

const userType = new GraphQLObjectType({
  name: "User",
  description: "User Type",
  fields: () => ({
    id: {
      type: GraphQLID,
      description: "MongoDB ID"
    },
    phoneNumber: {
      type: GraphQLString,
      description: "The Phone Number"
    },
    name: {
      type: GraphQLString,
      description: " Name of the User"
    },
    countryCode: {
      type: GraphQLString,
      description: "Country code of the phone number"
    },
    sex: {
      type: SEXtype,
      description: "Male/Female"
    },
    fbToken: {
      type: GraphQLString,
      description: "FB Token"
    },
    appId: {
      type: GraphQLString,
      description: "FB App ID"
    },
    accountID: {
      type: GraphQLString,
      description: "FB Account ID"
    },
    avator: {
      type: GraphQLString,
      description: "User Image basecode string"
    }
  })
});

const query = {
  users: {
    type: new GraphQLList(userType),
    args: {
      limit: {
        description: "limit items in the results",
        type: GraphQLInt
      }
    },
    resolve: (root, { limit }) => getUsers(limit)
  }
};

const userByPhone = {
  userByPhone: {
    type: userType,
    args: {
      phoneNumber: {
        description: "find by phone",
        type: GraphQLString
      }
    },
    resolve: (root, { phoneNumber }) => getUserByPhone(phoneNumber)
  }
};

const addUserMutation = {
  addUser: {
    type: userType,
    args: {
      email: {
        type: new GraphQLNonNull(GraphQLString)
      },
      password: {
        type: new GraphQLNonNull(GraphQLString)
      },
      firstName: {
        type: new GraphQLNonNull(GraphQLString)
      },
      lastName: {
        type: new GraphQLNonNull(GraphQLString)
      },
      sex: {
        type: new GraphQLNonNull(GraphQLString)
      },
      dob: {
        type: new GraphQLNonNull(GraphQLString)
      },
      verified: {
        type: new GraphQLNonNull(GraphQLBoolean)
      }
    },
    resolve: (obj, input) => addUser(input)
  }
};
const removeUserById = {
  removeUserById: {
    type: userType,
    args: {
      id: {
        type: new GraphQLNonNull(GraphQLID)
      }
    },
    resolve: (obj, id) => removeUser(id)
  }
};

const subscription = {};

export const UserSchema = {
  query,
  addUserMutation,
  userByPhone,
  subscription,
  removeUserById,
  types: [userType]
};
