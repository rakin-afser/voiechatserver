let crypto = require("crypto");
let algorithm = "aes-256-ctr",
  password = "reshare_cryption";

export function encrypt(text: string) {
  var cipher = crypto.createCipher(algorithm, password);
  var crypted = cipher.update(text, "utf8", "hex");
  crypted += cipher.final("hex");
  return crypted;
}

export function decrypt(hex: string) {
  var decipher = crypto.createDecipher(algorithm, password);
  var dec = decipher.update(hex, "hex", "utf8");
  dec += decipher.final("utf8");
  return dec;
}
